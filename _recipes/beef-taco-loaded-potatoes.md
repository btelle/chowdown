---

layout: recipe
title:  "Beef Taco Loaded Potatoes"
image: beef-taco-loaded-potatoes.jpg
imagecredit: https://www.hellofresh.com/recipes/beef-taco-loaded-potatoes-5b9178ac30006c62d04e31f2
tags: mains, beef, hello fresh
prep-time: 35 minutes

nutrition:
- servings: 2
- calories: 740
- fat: 42g
- carbohydrate: 50g
- sugar: 6g
- protein: 37g
- sodium: 480mg

ingredients:
- 10 oz ground beef
- 1 russet potato
- 1/2 cup mexican blend cheese
- 1 roma tomato
- 2 scallion
- 1 long green pepper
- 2 Tbsp sour cream
- 1 tsp chili powder
- 1 Tbsp southwest spice blend
- 1 tsp hot sauce
- 1 tsp olive oil
- 1 tsp vegetable oil
- 1 Tbsp butter
- salt
- pepper

directions:
- Wash and dry all produce. Remove stems and seeds from green pepper, then cut into ½-inch squares. Trim, then thinly slice scallions, separating greens and whites. Core and seed tomato, then cut into ¼-inch pieces.
- Using a fork, prick potato all over, then rub with a drizzle of olive oil. Season skin with salt and pepper. Place in a large microwave-safe bowl. Microwave on high for 5 minutes. Using tongs, carefully flip over potato in bowl. Continue microwaving until tender, 3-5 minutes more. Set aside to cool slightly.
- Meanwhile, heat a drizzle of oil in a large pan over medium-high heat. Add green pepper and scallion whites. Cook, stirring, until softened and lightly browned, 4-5 minutes. Push to one side of pan.
- Add beef, Southwest spice, chili powder, and a large pinch of salt and pepper to empty side of pan. Break up meat into pieces and cook until browned and cooked through, 4-5 minutes. (Try to keep the veggies and beef separate.) Drain any excess grease from pan, then toss veggies into beef. Add half the tomato to pan and cook, tossing, until starting to release juices, about 2 minutes. Season with plenty of salt and pepper. Remove pan from heat.
- Halve potato lengthwise once cool enough to handle. Using a fork, fluff insides of potato, keeping flesh inside skins. Mash 1 TBSP butter into fluffed potato (use ½ TBSP per potato half, using your fork to melt and combine).
- Divide potato halves between plates. Scatter beef and veggie mixture and remaining tomato over. Sprinkle with cheese. Dollop with sour cream and garnish with scallion greens. Drizzle with hot sauce to taste.

---

Talk about the best of both worlds: these spuds are one part loaded potato and one part taco fillings, which adds up to a food mashup that’s as genius as it is tasty. The most brilliant part of it might be where our chefs decided to skip the lengthy roasting usually used to make potatoes tender. Instead, they decided to zap them in the microwave to produce the same results in minutes. When the taters are filled with spiced ground beef, peppers, sour cream, and cheese, it’s pretty clear that this combo was truly meant to be.
