---

layout: recipe
title:  "Boursin Pasta"
image: boursin-pasta.jpg
imagecredit: https://www.tiktok.com/@sammymontgoms/video/7224960141836225818
tags: mains, pasta
prep-time: 20 mins

ingredients:
- 3 cups cherry tomatoes
- 1 package Boursin cheese
- 4-5 cloves garlic
- 1 container penne pasta
- fresh basil
- red pepper flakes
- salt
- pepper
- olive oil

directions:
- Cook pasta according to package instructions, reserving 1/2 cup of pasta water for sauce.
- Rinse tomatoes and place in large baking dish. Put the whole block of Boursin in the center of the baking dish. Sprinkle garlic cloves in the dish.
- Drizzle tomatoes and cheese with olive oil and season with red pepper flakes, salt and pepper.
- Bake until tomatoes are cooked and bursting, appx 20 mins.
- Crush tomatoes and garlic cloves. Add pasta water and mix everything together. Add fresh basil.
- Mix in cooked pasta. Serve topped with parmesan cheese.

---
