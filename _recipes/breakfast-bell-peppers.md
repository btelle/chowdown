---

layout: recipe
title:  "Breakfast Bell Peppers"
image: breakfast-bell-peppers.webp
imagecredit: https://tasty.co/recipe/breakfast-bell-peppers
tags: breakfast, eggs
prep-time: 40 minutes

nutrition:
- servings: 4
- calories: 474
- fat: 32g
- carbohydrate: 11g
- sugar: 5g
- protein: 34g

ingredients:
- 2 bell peppers
- 1/2 cup fresh chives, finely chopped
- 1 cup shredded mozzarella cheese
- 1/2 cup shredded cheddar cheese
- 4 large eggs
- 4 strips cooked bacon
- Salt
- Pepper

directions:
- Preheat oven to 350F.
- On a cutting board, split the bell peppers in half from stem to base. Cut out the stem and the seeds.
- Transfer halved peppers to a baking tray, cavity side up. Sprinkle each half with salt and pepper.
- Bake for 15-20 minutes, until peppers are slightly soft.
- Sprinkle the mozzarella and half of the chives evenly among the four pepper halves. Then crack eggs into the center of each pepper.
- Sprinkle the salt, pepper, bacon, cheddar, and remaining chives on top of the eggs.
- Bake for 15-20 minutes, until egg whites are set. Serve hot.

---

Simple stuffed bell peppers with breakfast staples: eggs, bacon, and cheese. Serve with potatoes, salsa and sour cream.
