---

layout: recipe
title:  "Caesar Dressing"
image: lumberjack-salad.jpg
tags: salads
prep-time: 5 mins

ingredients:
- 1/4 cup red wine vinegar
- 2 tbsp lemon juice
- 6 tbsp store-bought mayonnaise
- 1/4 cup packed grated parmesan cheese
- 2 tsp Dijon mustard
- 1 clove garlic
- 1/4 cup olive oil
- 1/2 tsp salt
- 1 1/2 tsp black pepper

directions:
- In a blender, combine the vinegar, lemon juice, mayonnaise, Parmesan, mustard, and garlic. Process on medium-high speed until smooth and well combined, 15 to 20 seconds. Stop the blender and scrape down the sides of the jar with a spatula as needed.
- With the blender running on low speed, slowly drizzle in the olive oil until the dressing is emulsified and thickened, about 30 seconds. Add the salt and pepper, adjusting the seasoning to taste.

---

This dressing will keep, refrigerated, for up to 7 days.
