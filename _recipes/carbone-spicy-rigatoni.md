---

layout: recipe
title:  "Carbone Spicy Rigatoni"
image: carbone-spicy-rigatoni.png
imagecredit: https://www.tiktok.com/@sammypur/video/7157064563978964266
tags: mains, pasta, tiktok
prep-time: 20 minutes

nutrition:
- servings: 3
- calories: 575
- fat: 20g
- carbohydrate: 54g
- sugar: 5g
- protein: 14g
- sodium: 700mg

ingredients:
- 1/2 box pasta
- 1/4 white onion, minced
- 1/2 Tbsp garlic paste
- 1/4 tsp Calabrian chili peppers
- 1/4 cup tomato paste
- 2 Tbsp vodka
- 1/3 cup heavy cream
- 1/3 cup fresh grated parmesan cheese
- olive oil
- butter
- salt

directions:
- Cook pasta according to box instructions. Retain 1/3 cup pasta water.
- Heat medium pan with 1 Tbsp olive oil over medium high heat. Cook onion until translucent, 2-3 mins
- Add garlic paste, Calibrian peppers, tomato paste. Stir to combine.
- Add vodka and stir. Get lit.
- Add heavy cream, salt, and butter, cook until butter is melted.
- Add pasta water and parmesan cheese. Stir. Add salt to taste.
- Add pasta to sauce and stir to combine. Serve topped with parmesan cheese.

---

I'm cancelling my reservation at Carbone because I figured out how to make the spicy rigatoni pasta at home. All ingredients available at Whole Foods. Pasta is Organic Pipe Rigate.
