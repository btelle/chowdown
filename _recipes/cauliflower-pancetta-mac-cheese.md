---

layout: recipe
title:  "Cauliflower Pancetta Mac & Cheese"
image: cauliflower-pancetta-mac-cheese.jpg
imagecredit: https://www.hellofresh.com/recipes/cauliflower-pancetta-mac-cheese-55c22e7ff8b25ef0058b4567
tags: mains, pancetta, hello fresh

ingredients:
- 4 oz pancetta
- 1 head cauliflower
- 6 oz shell pasta
- 8 oz Italian cheese blend
- 2 scallion
- 2 Tbsp flour
- vegetable stock concentrate
- 4 oz milk
- 1 tsp olive oil
- 2 Tbsp butter

directions:
- Preheat the oven to 400 degrees. Bring a large pot of water with a large pinch of salt to a boil. Once boiling, add the pasta to the water and cook for 8-10 minutes until al dente. Drain. Toss the drained pasta with 1 teaspoon olive oil to prevent it from sticking.
- Cut the cauliflower into bite-sized florets. Thinly slice the scallions, keeping the greens and whites separate.
- Toss the cauliflower on a baking sheet with 1 tablespoon olive oil and season with salt and pepper. Place the baking sheet in the oven for 20-25 minutes until golden brown and tender.
- Heat a medium pan over medium heat. Add the pancetta and scallion whites to the pan and cook, tossing, for 4-6 minutes until the pancetta is crispy. Set aside, leaving as much oil in the pan as possible.
- Make the cheese sauce -- heat 2 tablespoons butter in the same pan over medium heat. Once melted, add the flour into the butter and cook, whisking constantly, for 1-2 minutes. Very slowly whisk the milk into the pan a little bit at a time until combined. Add the stock concentrate to the pan and bring the mixture to a simmer for 1-2 minutes, until thickened. Remove the pan from the heat and stir in the cheese. Season with salt and pepper (to taste).
- Stir the pasta, pancetta mixture, and ¾ the cauliflower into the cheese sauce, then transfer mixture to a lightly oiled baking dish. Top with the remaining cauliflower, then transfer dish to the oven to bake for 5-7 minutes, until bubbling.
- Garnish the macaroni and cheese with scallion greens and enjoy!

---

Everyone has their own favorite spin on the classic mac and cheese, but prepare to make room on your roster for this one. When it comes to cooking, a little pancetta makes everything better. Reduced-fat milk and cauliflower make this comfort food winner just a bit more virtuous.
