---

layout: recipe
title:  "Ceviche Tostadas"
image: ceviche-tostadas.jpg
imagecredit: https://www.tiktok.com/@maxiskitchen/video/7230542103955246378
tags: mains, seafood
prep-time: 15 mins

ingredients:
- 1 red bell pepper, chopped
- 1/2 cup jicima, chopped
- 1 serrano pepper, finely chopped
- 1/2 red onion, chopped
- 1/2 avocado, chopped
- 1/2 cup corn
- small handfull fresh cilantro, diced
- cooked shrimp, optional
- tortilla chips or tostadas
- 3 limes
- 1 clove garlic
- olive oil
- salt
- pepper

directions:
- In a small bowl, combine the juice of the 3 limes, 3 Tbsp olive oil, salt, pepper and grated garlic
- Combine veggies and shrimp (if desired) in a large bowl.
- Season with salt and pepper, toss with dressing.
- Serve with tortilla chips or tostadas.

---
