---

layout: recipe
title:  "Cheesy Beef Tostadas"
image: cheesy-beef-tostadas.jpg
imagecredit: https://www.hellofresh.com/recipes/cheesy-beef-tostadas-5b916c9eae08b575ac470482
tags: mains, beef, mexican, hello fresh
prep-time: 20 minutes

nutrition:
- servings: 2
- calories: 870
- fat: 48g
- carbohydrate: 61g
- sugar: 8g
- protein: 37g
- sodium: 1680mg

ingredients:
- 10 oz ground beef
- 6 flour tortilla
- 1/2 cup mexican blend cheese
- 1 yellow onion
- 1/4 oz cilantro
- 1 green bell pepper
- 1 lime
- 4 Tbsp sour cream
- 1 tsp chili powder
- 1 Tbsp southwest spice blend
- 1 tsp hot sauce
- beef stock concentrate
- 1 tsp hot sauce
- 1 tsp olive oil
- salt
- pepper

directions:
- Wash and dry all produce. Preheat broiler to high with rack in position closest to flame. Heat a drizzle of olive oil in a large pan over medium-high heat. Add beef, Southwest spice, chili powder, and a few big pinches of salt. Break up meat into smaller pieces. Cook, tossing occasionally, until starting to brown, about 5 minutes.
- While beef cooks, core and seed green pepper. Halve lengthwise, then slice into thin strips. Halve and peel onion; thinly slice one half (save the other for the next step). Once beef browns, add green pepper and onion slices to same pan along with a pinch of salt. Cook, tossing, until veggies are tender, about 5 minutes.
- Pour 1/4 cup water and stock concentrate into pan. Simmer until most of the liquid evaporates, about 1 minute. Season with salt, then remove pan from heat and set aside. Cut tomato into small cubes. Roughly chop cilantro. Cut lime into quarters. Very finely chop enough of the remaining onion to give you 2 TBSP (use the rest as you like).
- Place tomato, cilantro, chopped onion, juice from two lime quarters, and a pinch of salt and pepper in a medium bowl. Toss to combine and set aside. In a separate small bowl, stir together sour cream and as much hot sauce as you like. Stir in water 1 tsp at a time until mixture has a drizzling consistency. Season with salt and set aside.
- Drizzle tortillas on all sides using 1 TBSP olive oil and brush to coat evenly. Arrange in a single layer on a baking sheet. Gently prick each tortilla in a few places with a fork. Place under broiler and broil until lightly toasted on top, about 2 minutes. Flip tortillas over with tongs or a spatula. Return to broiler and toast on other side, about 2 minutes more. Keep an eye out for any burning the entire time.
- Divide tortillas between plates. Evenly scatter cheese onto top of each tortilla. Top with beef mixture. Dollop with salsa and crema. Serve with remaining lime quarters on the side.

---

We love a good ground-beef taco, but you don’t need us to tell you how to make one—there are kits for that. Instead, we’re here to show you how to take Tex-Mex night to the next level. So enter the tostada, featuring a flat tortilla baked until crisp, with lots of surface area for piling it on. Thankfully, there are plenty of toppings in this recipe, and not just warmly spiced ground beef—there’s also green pepper, tomato salsa, and a spicy sour cream.
