---

layout: recipe
title:  "Chicken Cheddar Fajitas"
image: chicken-cheddar-fajitas.jpg
imagecredit: https://www.hellofresh.com/recipes/chicken-cheddar-fajitas-5964eb582c9e8642ab4ac4d3
tags: mains, chicken, fajitas, hello fresh
prep-time: 40 minutes

nutrition:
- servings: 2
- calories: 720
- fat: 29g
- carbohydrate: 57g
- sugar: 6g
- protein: 54g
- sodium: 790mg

ingredients:
- 6 flour tortillas
- 12 oz chicken breasts
- 1/2 cup cheddar cheese
- 1 red bell pepper
- 1 red onion
- 1 jalepenos
- 1 lime
- 1 tsp southwest spice blend
- 2 Tbsp sour cream
- 1 Tbsp vegetable oil
- salt
- pepper

directions:
- Wash and dry all produce. Preheat oven to 400 degrees. Core and seed bell pepper, then thinly slice. Halve, peel, and thinly slice onion. Thinly slice the jalapeño into rounds, removing ribs and seeds for less heat, and place in a small bowl. Zest lime until you have ½ tsp zest, then cut into halves. Cut one half into wedges.
- Squeeze juice from lime half into bowl with jalapeño and toss to coat. Set aside to marinate. Heat a drizzle of oil in a large pan over medium-high heat. Add onion, bell pepper, and 1 tsp Southwest spice (we sent more). Cook until softened and lightly charred, 4-5 minutes, tossing. Season with salt and pepper. Remove from pan and set aside.
- In another small bowl, mix together sour cream, lime zest, and a squeeze of lime juice. Stir in 1 TBSP water; add up to 1 TBSP more water to give crema a drizzly consistency. Season with salt and pepper.
- Heat a large drizzle of oil in same pan over medium-high heat. Thinly slice chicken into strips. Season with salt and pepper. Add to pan in a single layer and cook until just browned on surface, 1-2 minutes per side. Toss in veggies and another 1 tsp Southwest spice (you’ll have a little left over). Continue cooking until chicken is cooked through, 2-3 minutes more.
- Meanwhile, place tortillas on a baking sheet in a single layer. Sprinkle evenly with cheddar. Bake in oven until cheese is just melted, 1-2 minutes. Don’t let these sit in the oven too long. The tortillas should be soft, not crisp and toasted.
- Divide chicken mixture between tortillas. Dollop with crema and scatter over jalapeño (to taste). Serve any remaining lime wedges on the side for squeezing over.

---

Fajitas are like a good friend—the one who never seems to change or age. Their combination of seared strips of meat, lightly charred veggies, and Tex-Mex-style toppings inside a soft tortilla is as reliable as can be; they’ll always be there for you when you don’t know what to do (about dinner, that is). Our chicken version is just as familiar and comforting as you expect, although we couldn’t help throwing in some pickled jalapeños to keep things interesting.
