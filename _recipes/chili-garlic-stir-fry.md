---

layout: recipe
title:  "Chili Garlic Stir Fry"
image: chili-garlic-stir-fry.jpg
imagecredit: https://www.tiktok.com/@dansfoodforthought/video/7181337019187023106
tags: mains, pasta
prep-time: 20 mins

ingredients:
- 1 clove garlic
- 1 tsp chili flake
- 2 green onion
- 2 tbsp soy sauce
- sesame seeds
- 1 tsp sesame oil
- 1 tsp honey
- noodles (ramen or egg)
- mushrooms
- red bell pepper
- zucchini
- red onion
- snow peas
- cabbage
- olive oil

directions:
- In a wok, heat olive oil with minced garlic, green onion whites, and chili flakes.
- In a small bowl, combine sesame oil, soy sauce and honey. Mix.
- Add sliced veggies. Cook, tossing frequently, until tender (appx 10 mins). Add cabbage last, cook for another minute.
- Add cooked noodles and sauce, toss to combine and let sauce simmer for a few minutes.
- Serve topped with green onions and sesame seeds. Optionally, serve with a fried egg.

---
