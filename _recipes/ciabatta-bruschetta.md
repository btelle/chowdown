---

layout: recipe
title:  "Ciabatta Bruschetta"
image: bruschetta.jpg
tags: lunch, snack
prep-time: 10 mins

ingredients:
- Ciabatta rolls
- Marinated mozzarella (Trader Joes)
- Bruschetta topping (Trader Joes)
- Balsamic glaze

directions:
- Slice rolls in half. Drizzle oil from mozzarella package on open side of each half.
- Chop up 2-3 balls of mozarrella and spread on each piece of roll.
- Air fry for 5-7 minutes, until cheese is melted and browning.
- Spread appx 1 Tbsp of bruschetta topping on each piece. Drizzle with balsamic glaze and serve warm.

---

Easy air fryer recipe for a quick lunch or snack.
