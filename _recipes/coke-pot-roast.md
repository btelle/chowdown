---

layout: recipe
title:  "Coke Pot Roast"
image: coke-pot-roast.jpg
imagecredit: https://www.thespruceeats.com/slow-cooker-pepsi-pot-roast-3055592
tags: mains, beef, slow cooker
prep-time: 8 hr

nutrition:
- servings: 6
- calories: 742
- fat: 45g
- carbohydrate: 10g
- sugar: 6g
- protein: 70g
- sodium: 550mg

ingredients:
- 3- to 4-pound pot roast, chuck, bottom round, or rump roast
- 1 package dry onion soup mix 
- 12 oz Coca Cola
- 1 can condensed cream of mushroom soup
- 1 yellow onion
- 2 cups small red potatoes
- 2 carrots
- 2 tablespoons all-purpose flour
- salt
- pepper

directions:
- Place the roast in a slow cooker that has been sprayed lightly with cooking spray (or use a plastic slow-cooker liner) to make cleanup easier. Season with salt and pepper.
- Rough-chop veggies and add to slow cooker.
- In a small bowl, whisk together onion soup mix, mushroom soup, and cola until well combined.  Pour over the roast.
- Cover and cook for 8 to 10 hours on low, or about 3 to 4 hours on high.
- To make gravy, whisk together 2 tablespoons of flour with 1/4 cup of cold water until smooth. Remove the pot roast from the liquids and keep warm. Pour the flour mixture into the liquids, whisking well, and continue cooking on high in the slow cooker—or in a saucepan on the stovetop over medium heat—until thickened. Serve with the meat. 

---

This easy slow-cooker pot roast has a secret ingredient—Pepsi—but if you don't have that brand, any other cola will work. Some pundits believe the acid in cola helps to tenderize a tough cut of meat but, more probably, a long, slow cook is what does it.

Add some sliced onions, carrots, celery, or potatoes to the pot roast for a complete meal. Cook it until it's tender and serve with gravy made by thickening the cooking juices. 
