---

layout: recipe
title:  "Creamy Chive Chicken"
image: creamy-chive-chicken.jpg
imagecredit: https://www.hellofresh.com/recipes/creamy-chive-chicken-5dc047615197ee55a945fc4f
tags: mains, chicken, salad, hello fresh
prep-time: 30 minutes

nutrition:
- servings: 2
- calories: 640
- fat: 31g
- carbohydrate: 52g
- sugar: 9g
- protein: 39g
- sodium: 470mg

ingredients:
- 2 chicken breasts
- 1/2 cup rice
- 1 tsp Better than Boullion chicken stock concentrate
- 2 tsp dijon mustard
- 1 lemon
- 1 apple
- 2 Tbsp sour cream
- 1/4 oz chives
- 2 oz mixed greens
- 5 tsp olive oil
- 1/2 tsp sugar
- 1 Tbsp butter
- salt
- pepper

directions:
- Wash and dry all produce. Halve and core apple; quarter one half lengthwise, then thinly slice crosswise into triangles (quarter and slice whole apple for 4 servings). Mince chives. Zest and quarter lemon (quarter both lemons for 4).
- In a small pot, combine rice, ¾ cup water (1 1/2 cups water for 4 servings), and a pinch of salt. Bring to a boil, then cover and reduce to a low simmer. Cook until rice is tender, 15-18 minutes. Keep covered off heat until ready to serve.
- While rice cooks, in a medium bowl, combine 1 TBSP olive oil (2 TBSP for 4 servings), juice from half the lemon, half the mustard, ½ tsp sugar (1 tsp for 4), salt, and pepper. Stir in half the chives. Set aside.
- Pat chicken dry with paper towels; season all over with salt and pepper. Heat a large drizzle of olive oil in a large, preferably nonstick, pan over mediumhigh heat. Add chicken and cook until browned and cooked through, 5-7 minutes per side. Turn off heat; transfer to a plate. Wipe out pan.
- Heat pan used for chicken over medium-high heat. Add stock concentrate, remaining mustard, 1/4 cup water (1/3 cup for 4 servings), and a squeeze of lemon juice. Simmer until slightly thickened, 1-2 minutes. Turn off heat. Stir in sour cream and remaining chives. ( If sauce seems too thick, add more water 1 tsp at a time until it reaches drizzling consistency.) Season with salt and pepper.
- Fluff rice with a fork; stir in 1 TBSP butter (2 TBSP for 4 servings) and lemon zest. Season with salt and pepper. Add mixed greens and apple to bowl with vinaigrette. Season with salt and pepper; toss to combine. Divide chicken, rice, and salad between plates. Drizzle chicken with sauce. Cut remaining lemon into wedges and serve on the side.

---

We’re big fans of pan sauces because they’re an easy way to add tons of restaurant-level flavor to your dish. Here, tangy lemon juice, punchy dijon, sour cream, and fresh chives mingle to create a condiment that truly takes seared chicken to the next level. On the side, there’s zesty lemon rice and a green salad that’s bursting with crispy pieces of apple... because you only deserve the best!
