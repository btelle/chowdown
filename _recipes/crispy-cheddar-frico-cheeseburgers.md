---

layout: recipe
title:  "Crispy Cheddar Frico Cheeseburgers"
image: crispy-cheddar-frico-cheeseburgers.jpg
imagecredit: https://www.hellofresh.com/recipes/w19r6-58d3c71ec6243b74f42cfe93
tags: mains, burgers, beef, hello fresh
prep-time: 35 minutes

nutrition:
- servings: 2
- calories: 740
- fat: 38g
- carbohydrate: 61g
- sugar: 15g
- protein: 39g
- sodium: 730mg

ingredients:
- 10 oz ground beef
- 1/2 cup cheddar cheese
- 2 brioche buns
- 1 red onion
- 1 roma tomato
- 1 Tbsp sherry vinegar
- 8 oz broccoli florets
- 2 Tbsp ketchup
- 1 Tbsp olive oil
- 1 tsp sugar
- salt
- pepper

directions:
- Wash and dry all produce. Preheat oven to 425 degrees. Halve, peel, and thinly slice onion. Slice tomatoes into rounds. Split buns in half. Shape beef into four evenly sized patties.
- Toss broccoli on a baking sheet with a large drizzle of olive oil and a pinch of salt and pepper. Roast in oven until slightly crispy, about 15 minutes.
- Heat a large drizzle of olive oil in a large pan over medium-high heat. Add onion and cook, tossing, until soft, 5-6 minutes. Stir in sherry vinegar and 2 tsp sugar. Simmer until liquid is nearly evaporated, 1-2 minutes. Season with salt and pepper. Transfer to a bowl and set aside.
- Line another baking sheet with parchment paper. Place cheddar on it in four even piles. Bake in oven until melted in middle and crispy at the edges, 5-7 minutes. If you don’t have parchment, that’s OK. As soon as the frico comes out of the oven, transfer it to a plate to cool using a spatula.
- Meanwhile, heat a large drizzle of olive oil in same large pan over mediumhigh heat. Season beef patties all over with salt and pepper. Add to pan and cook to desired doneness, 3-6 minutes per side. Meanwhile, carefully remove frico from baking sheet, then place buns on sheet. Toast in oven until golden, 3-4 minutes.
- Place burgers, caramelized onion jam, tomato slices, ketchup, and a cheddar frico inside each bun. Serve with crispy broccoli on the side.

---

We’ve taken cheeseburgers to the next level with the help of a little something called frico (aka cheese crisps). It looks fancy-schmancy, but making it simply involves baking cheddar cheese until it reaches crispy, golden-brown perfection. On top of a burger with onion jam, tomatoes, and ketchup, it feels like a crowning achievement in topping technology.
