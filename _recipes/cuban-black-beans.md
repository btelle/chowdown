---

layout: recipe
title:  "Cuban Black Beans"
image: cuban-black-beans.jpg
imagecredit: https://littlespicejar.com/cuban-black-beans-cilantro-lime/
tags: sides, cuban
prep-time: 4 hours

nutrition:
- servings: 4
- calories: 195
- fat: 1g
- carbohydrate: 35g
- sugar: 5g
- protein: 9g

ingredients:
- 1 medium yellow onion, diced
- 1 green bell pepper, diced
- 1 jalapeños, seeded and diced
- 4 cloves garlic, minced
- 2 cans black beans, rinsed and drained
- 1 cup warm water
- 1 tsp salt
- 1 tsp ground cumin
- 1/2 tsp chipotle chili powder (optional)
- 2 Tbsp cilantro
- 1 tsp oil
- 1 Tbsp lime juice


directions:
-  Heat the oil in a stockpot or dutch oven over medium-high heat. Sauté the onions and peppers for 4-5 minutes or until they turn translucent. Add the garlic and continue to cook for 30 seconds.
- Add the black beans, warm water, salt, cumin, and chili powder and bring to a boil. When boiling, reduce to a simmer.
- Allow to cook for 10-15 minutes or until most of the water evaporates leaving a thick sort of broth. 
- Remove about 1/2 cup of beans and mash them. Add the beans back into the pot and stir.
- Top with fresh cilantro and lime juice, stir to combine.

---

A quick and easy version of the Cuban black beans! I’m sautéing up jalapeños, onions, and garlic and then simmering the beans until they are tender and topping them with cilantro and lime! So, so delicious!