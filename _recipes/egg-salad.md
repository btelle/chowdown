---

layout: recipe
title:  "Egg Salad"
image: egg-salad.jpg
imagecredit: https://www.tiktok.com/@caileeeats/video/7241692181692468522
tags: lunch, eggs
prep-time: 10 mins

ingredients:
- 4 hardboiled eggs
- red onion, chopped
- 1/2 avocado
- green onion
- 1/2 tsp lemon juice
- 1 Tbsp mayonnaise
- 1 tsp dijon mustard
- salt
- pepper
- paprika

directions:
- Chop eggs, onion, avocado and add to small bowl.
- Add wet ingredients and season to taste, mix well to combine.
- Serve on toast, topped with green onion, or in a sandwich.

---
