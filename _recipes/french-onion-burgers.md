---

layout: recipe
title:  "French Onion Burgers"
image: french-onion-burgers.jpg
imagecredit: https://www.hellofresh.com/recipes/french-onion-burgers-59ce4b9e05346814eb3a6822
tags: mains, burgers, beef, hello fresh
prep-time: 30 minutes

nutrition:
- servings: 2
- calories: 860
- fat: 49g
- carbohydrate: 57g
- sugar: 14g
- protein: 46g
- sodium: 940mg

ingredients:
- 10 oz ground beef
- 1/2 cup gruyère cheese
- 2 brioche buns
- 1 yellow onion
- 1/2 cup milk
- 1 Tbsp flour
- 0.13 tsp nutmeg
- 1/4 oz thyme
- 4 oz kale
- beef stock concentrate
- 1 Tbsp olive oil
- 1 Tbsp butter
- 1 tsp sugar
- salt
- pepper

directions:
- Wash and dry all produce. Preheat oven to 350 degrees. Halve, peel, and thinly slice onion. Remove stems and large ribs from kale, then tear leaves into bite-sized pieces. Toss on a baking sheet with a drizzle of olive oil and a pinch of salt and pepper. Bake in oven until crisp, 10-12 minutes. Keep an eye on these—they can burn quickly.
- Heat a drizzle of olive oil in a large pan over medium heat. Add onion and thyme sprigs. Cook, tossing, until soft and browned, 7-8 minutes. Stir in stock concentrate, 1 tsp sugar, and ¼ cup water. Simmer until onion is jammy, 2-3 minutes. Remove and discard thyme sprigs. Remove onion from pan and set aside. Wipe out pan.
- Meanwhile, grate cheese on large holes of a grater. Melt 1 TBSP butter in a small pot over medium heat. Add flour and stir until lightly toasted, about 1 minute. Slowly whisk in ½ cup milk (we sent more), then simmer until thickened, 1-2 minutes. Season with a pinch of nutmeg (we sent more than needed), salt, and pepper. Set aside off heat.
- Shape beef into two evenly sized patties (they should be slightly larger than the buns). Season all over with salt and pepper. Heat a drizzle of oil in pan used for onion over medium-high heat. Add burgers and cook until just shy of desired doneness, 3-5 minutes per side. Sprinkle cheese on top of each burger, cover pan, and cook until cheese melts, 1-2 minutes.
- While burgers cook, split buns half and place on a baking sheet. Toast in oven until golden, 2-3 minutes.
- Spread béchamel onto buns, then fill with burgers and onion. Serve with kale chips to the side.

---

This is the story of when hamburgers met French onion soup. At first, their pairing seemed unlikely. How could two iconic dishes with big personalities possibly get along? But when they set aside their differences and agreed to meet, the results were pure harmony. Beefy patties, sweet onions, a creamy sauce, and cheese—oh, the bliss! And things only got more interesting when their friend kale chips came along and joined in on this tasty affair.
