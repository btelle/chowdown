---

layout: recipe
title:  "Fried Tilapia Sandwiches"
image: fried-tilapia-sandwiches.jpg
imagecredit: https://www.hellofresh.com/recipes/fried-tilapia-sandwiches-5b6e032630006c0179064b02
tags: mains, fish, hello fresh

ingredients:
- 2 brioche buns
- 11 oz tilapia fillets
- 1 persian cucumber
- 2 scallions
- 4 oz shredded red cabbage
- 1/2 cup panko breadcrumbs
- 2 Tbsp sour cream
- 5 tsp white wine vinegar
- 6 Tbsp hot sauce
- 2 tsp olive oil
- 1 Tbsp fry seasoning
- 1 tsp sugar
- 1 Tbsp butter
- 3/4 cup vegetable oil
- salt
- pepper

directions:
- Wash and dry all produce. Split buns in half and set aside. Thinly slice cucumbers on a diagonal. Trim, then thinly slice scallions. Toss scallions, cucumber, cabbage, vinegar, 1 tsp sugar, and a large drizzle of olive oil in a medium bowl. Season with salt and pepper.
- Melt 1 TBSP butter in a large pan over medium-high heat (use nonstick if you have it). Add buns cut-side down and toast until golden, 2-3 minutes. Set aside.
- While buns toast, in a shallow dish, stir together panko, ½ tsp salt, and 2 tsp fry seasoning (save the last teaspoon for step 5). Brush 2 TBSP sour cream (1 packet) onto tilapia, coating all over. Season with salt and pepper. Dip each fillet in panko mixture, coating all over and pressing to adhere.
- Heat a 1/8-inch layer of oil in pan used for buns over medium-high heat (we used about 3/4 cup oil). Add tilapia to pan and cook until panko is golden and fish is cooked through, 3-4 minutes per side.
- Stir together remaining sour cream, remaining fry seasoning, and half the hot sauce in a small bowl. Season with salt and pepper.
- Cut tilapia into 3-inch pieces. Spread sauce on cut side of buns, then fill buns with tilapia. Pick out a few cucumbers from slaw and add to sandwiches. Drizzle with remaining hot sauce to taste. Divide between plates and serve with slaw on the side.

---

Just in time for fish fry-day: breaded tilapia sandwiches on brioche buns! These crispy, hot creations will always be a win in our book, although golden-crusted fillets seem like an especially good way to celebrate the end of a week. They’re made all the more special with the addition of a creamy and spicy sauce plus a cool slaw on the side to keeps things snappy.
