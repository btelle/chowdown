---

layout: recipe
title:  "Grinder Sandwich"
image: grinder-sandwich.jpg
imagecredit: https://www.tiktok.com/@karikattie/video/7161085398444182827
tags: lunch, sandwich, vegetarian
prep-time: 15 mins

ingredients:
- hoagie rolls
- sliced cheese (havarti or provolone preferred)
- 1/4 cup mayonnaise
- 1/4 cup red wine vinegar
- 2 Tbsp chopped banana peppers
- sliced turkey (optional)
- 2 cups chopped lettuce (butter lettuce preferred)
- thin-sliced red onion
- large tomato
- olive oil
- salt
- pepper
- oregano

directions:
- Prep the hoagie by drizzling some olive oil on each side and putting a slice of cheese on one side. Air fry open-faced for 3-5 minutes, until bread is toasted and cheese is melted.
- Combine mayonnaise, vinegar, banana peppers and seasonings in large bowl.
- Add lettuce and onion to the bowl, toss to coat.
- Slice tomatoes and season.
- Stack tomatoes, (optionally) turkey, and a generous amount of lettuce mixture on the hoagie.

---
