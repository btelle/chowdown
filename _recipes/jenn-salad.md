---

layout: recipe
title:  "\"Jenn\" Salad"
image: jenn-salad.jpg
tags: mains, pasta
prep-time: 20 mins

ingredients:
- 1 cup cooked quinoa, chilled
- 1/2 can garbanzo beans, rinsed
- pickled red onion to taste
- 1 persian cucumber, diced
- 1/4 cup shredded carrot, diced
- 1/8 cup feta, optional
- 1 tsp parsley
- olive oil to taste
- salt
- pepper

directions:
- To pickle onion, boil equal parts red wine vinegar and rice vinegar with 1 Tbsp sugar. Place thinly sliced red onion in container and cover with hot vinegar mixture. Allow to pickle for at least 10 minutes, but the longer the better. Stays good refrigerated in an airtight container for 2 days.
- Combine all ingredients in a large bowl and toss until coated with olive oil and well mixed. Serve cold.

---

Inspired by the salad Jennifer Anniston allegedly ate frequently on the set of Friends, although she has refuted this and said it was a Cobb.
