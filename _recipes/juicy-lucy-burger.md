---

layout: recipe
title:  "Juicy Lucy Burger"
image: juicy-lucy-burger.jpg
imagecredit: https://www.hellofresh.com/recipes/15-02-58af2408d0d6bd58052dcee4
tags: mains, pancetta, hello fresh
prep-time: 45 minutes

nutrition:
- servings: 2
- calories: 840
- fat: 50g
- carbohydrate: 57g
- sugar: 18g
- protein: 43g
- sodium: 670mg

ingredients:
- 2 brioche buns
- 10 oz ground beef
- 1/2 cup cheddar cheese
- 1 roma tomato
- 1 red onion
- 2 Tbsp balsamic vinegar
- 2 oz arugula
- 1 Tbsp mayonnaise
- 2 Tbsp ketchup
- 2 clove garlic
- 4 Tbsp olive oil
- 1 tsp sugar
- salt
- pepper

directions:
- Wash and dry all produce. Preheat oven to 400 degrees. Core, seed, and dice tomato. Peel, halve, and thinly slice onion. Wrap garlic cloves in foil, sealing to make a pouch, and roast in oven until very soft, about 20 minutes.
- Heat a drizzle of oil in a large pan over medium heat. Add onion and cook, tossing, until softened, 4-5 minutes. Season with salt and pepper. Stir in tomato, 1 tsp sugar, and 1 TBSP balsamic vinegar (we’ll be using the rest later). Cook until everything is soft and jammy, 4-5 minutes. Season with salt and pepper. Remove mixture from pan and set aside
- Divide ground beef in half and flatten each piece into a wide, roughly ½-inch-thick circle. Place half the cheddar in the center of each circle. Fold edges of meat around cheese, shaping and sealing to create a cheese-stuffed patty. Season all over with salt and pepper.
- Carefully wipe out pan used for onions with a paper towel. Add a drizzle of oil and heat over medium-high heat. Place burgers in pan and cook to desired doneness, 3-5 minutes per side. Don’t worry if some of the cheese leaks out. It’ll still be delicious!
- While burgers cook, halve buns and place on a baking sheet. Toast in oven until golden brown, 2-3 minutes. Once garlic is done roasting, mash with a fork until smooth. In a small bowl, combine ½ TBSP mayonnaise (you’ll be using more later), remaining balsamic vinegar, a large drizzle of oil, and as much garlic as you like. Season with salt and pepper.
- Spread buns with ketchup and ½ TBSP mayo (you’ll have some left over). Divide burgers between buns and top with onion jam and a small handful of arugula. Toss remaining arugula with dressing and serve on the side.

---

The Juicy Lucy burger is one that takes you by surprise. It looks unassuming from the outside, but hidden deep in its patty is a core of molten cheese just waiting to gush out. Get your napkins ready, and make sure to have the side spring salad at the ready for cleansing your palate in between bites of cheddar-filled, beefy decadence.
