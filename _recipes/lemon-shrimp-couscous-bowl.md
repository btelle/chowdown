---

layout: recipe
title:  "Lemon Shrimp Couscous Bowl"
image: lemon-shrimp-couscous-bowl.jpg
tags: mains, seafood
prep-time: 30 mins

ingredients:
- pre-cooked shrimp
- 1 cup cauliflower
- 1 lemon
- pearl couscous
- sundried tomatoes, jarred
- apple cider vinegar
- balsamic glaze
- arugala

directions:
- Chop Cauliflower, drizzle with olive oil, salt, pepper. Chop lemon in half, drizzle with olive oil. Place on baking sheet, bake at 400F for 20 mins
- Marinate shrimp in olive oil, garlic, salt + pepper.
- On stove, make pearl couscous. Add sundried tomato oil after cooked
- Make room on baking sheet, place shrimp on sheet, back into oven for 8-10 mins
- Squeeze lemon, apple cider vinegar, olive oil, salt. Mix together
- Assemble. Couscous, cauliflower, sundried tomatoes, feta, any herbs (arugala). Add shrimp, drizzle with balsamic glaze
---

