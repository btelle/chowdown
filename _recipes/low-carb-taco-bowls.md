---

layout: recipe
title: "Low-carb Taco Bowls"
image: low-carb-taco-bowls.jpg
tags: lunch, make-ahead, low-carb

ingredients:
- 1 lb ground turkey
- 1 can refried beans
- 4 oz shredded cheese
- 1 packet taco seasoning
- pickled jalepenos
- hot sauce
- sour cream

directions:
- Cook turkey in a skillet according to instructions on the taco seasoning packet
- Distribute uncooked beans and turkey into 4-5 tupperware containers. Top with cheese, hot sauce and jalepenos. Refrigerate for up to 1 week.
- To reheat, microwave for 1-2 minutes until hot. Top with sour cream (optional)

---
