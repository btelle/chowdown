---

layout: recipe
title:  "Lumberjack Salad"
image: lumberjack-salad.jpg
tags: salads
prep-time: 20 mins

ingredients:
- 4 cups romaine lettuce, chopped
- 1/2 avocado, diced
- 1/4 cup cherry tomatoes, halved
- 1/4 roasted chicken breast
- 2 slices bacon, cut into pieces and fried until crispy
- 1/4 cup chopped sliced turkey
- 1/4 cup shredded mozzarella cheese
- 1/4 cup sliced scallion, green part only
- 1/2 cup pita chips
- 1/3 cup caeser dressing

directions:
- Combine all ingredients in a large bowl. Top with the dressing and toss until well mixed and dressed.

---
