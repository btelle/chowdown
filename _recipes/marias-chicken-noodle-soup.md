---

layout: recipe
title: "Maria's Chicken Noodle Soup"
image: marias-chicken-noodle-soup.jpg
tags: soup, make-ahead

ingredients:
- 1 rotisserie chicken, chopped (optional)
- 1 can white corn
- 1 can peeled potatoes, quartered
- 1/2 package frozen peas/carrots
- 1/2 package frozen squash
- 1/2 package Italian fideo noodles (or sub angel hair pasta)
- 6 cups chicken broth or bouillon cubes

directions:
- Boil chicken broth. If using bouillon, use one cube per 2 cups of water.
- Add frozen squash, cook until soft. 
- Add potatoes, cook until soft.
- Add frozen vegetable mix and corn, cook until tender.
- Add chicken if using chicken.
- Add noodles. If using angel hair, break up into ~2 inch pieces. Cook until tender.

---
