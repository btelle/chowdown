---

layout: recipe
title:  "\"Marry Me\" Tuscan Chicken"
image: marry-me-tuscan-chicken.jpg
tags: mains, chicken
prep-time: 20 mins

ingredients:
- 4 (6-ounce) boneless skinless chicken breasts
- 1 teaspoon Italian seasoning, or a mix of individual spices such as oregano, thyme, basil, and rosemary
- 1 teaspoon kosher salt
- 1/2 teaspoon freshly ground black pepper
- 3 tablespoons extra virgin olive oil
- 4 garlic cloves, minced
- 1/4 cup finely chopped yellow onion
- 1/2 cup sun-dried tomatoes, thinly sliced
- 2 cups baby spinach
- 1 1/2 cups heavy cream, *see note above for dairy-free options
- 1/4 cup freshly grated parmesan

directions:
- Prep the chicken. Season both sides of each chicken breast in Italian seasoning, salt, and pepper
- Sear the chicken. Heat 2 tablespoons of the oil in a large skillet over medium heat. Add the chicken and sear for 3 to 4 minutes on each side, until golden. Remove the chicken from the pan to a plate and set aside
- Cook the onions. In the same skillet, add another tablespoon of oil and the onion. Saute for 2 to 3 minutes, until the onion has softened.
- Add the sun-dried tomatoes and garlic and saute another minute, until fragrant.
- Add the spinach, and saute another minute, until just starting to wilt.
- Add the heavy cream and parmesan cheese, stir together, and bring to a simmer.
- Let it simmer. Place the chicken back in the skillet and cook until heated through, about 5 minutes.

---
