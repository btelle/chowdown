---

layout: recipe
title:  "Melty Mozzarella Burgers"
image: melty-mozzarella-burgers.jpg
imagecredit: https://www.hellofresh.com/recipes/melty-mozzarella-burgers-5a6f48d6ae08b57fff575942
tags: mains, beef, burgers, hello fresh
prep-time: 30 minutes

nutrition:
- servings: 2
- calories: 750
- fat: 37g
- carbohydrate: 66g
- sugar: 19g
- protein: 39g
- sodium: 780mg

ingredients:
- 2 ciabatta buns
- 10 oz ground beef
- 1/2 cup mozzarella cheese
- 1 clove garlic
- 1/2 Tbsp tomato paste
- 3 tsp balsamic vinegar
- 4 oz hierloom grape tomato
- 2 oz spring mix lettuce
- 1 oz dried cranberry
- 5 tsp olive oil
- 1 tsp sugar
- salt
- pepper

directions:
- Wash and dry all produce. Preheat toaster oven or oven to 400 degrees. Halve, peel, and thinly slice onion. Heat a drizzle of olive oil in a large pan over medium heat. Add onion and 1 tsp sugar. Cook, tossing, until browned, 6-8 minutes. Season with salt and pepper. Remove from pan and set aside.
- While onion cooks, mince or grate garlic until you have ½ tsp (you may have a clove left over). In a large bowl, whisk together ½ TBSP tomato paste, 1 TBSP vinegar, 1 TBSP olive oil, and as much minced garlic as you like (we sent more tomato paste and vinegar than needed). Season with salt and pepper.
- Wipe out pan used for onion. Heat a drizzle of olive oil in it over medium-high heat. Shape beef into 2 patties slightly wider than the bread. Season with salt and pepper. Add to pan and cook until just shy of desired doneness, 2-4 minutes per side. Sprinkle mozzarella on top. Cover pan and cook until cheese melts, 1 minute more.
- Split ciabattas in half. Toast in toaster oven or oven until golden, 3-5 minutes. Meanwhile, halve tomatoes lengthwise.
- Add lettuce, tomatoes, and cranberries to bowl with vinaigrette and toss to combine. Season with salt and pepper.
- Fill ciabattas with beef patties, onion, and a few leaves from salad. Divide between plates and serve with remaining salad on the side.

---

Burgers are about as all-American as you can get. But that doesn’t mean they don’t have dreams of voyaging to faraway places. These patties on ciabatta are what happens when cheeseburgers take a trip to Italy. Between the sweetness of the caramelized onion on top, the way the mozzarella melts into loose strands, and the tangy kick brought by balsamic vinegar dressing, you could say these burgers are loaded with dolce vita attitude.
