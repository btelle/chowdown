---

layout: recipe
title:  "Olive Garden Chicken Gnocchi Soup"
image: olive-garden-chick-gnocchi-soup.jpg
imagecredit: https://www.lecremedelacrumb.com/olive-garden-chicken-gnocchi-soup/
tags: mains, soup, slow cooker
prep-time: 4 hours

nutrition:
- servings: 4
- calories: 523
- fat: 22g
- carbohydrate: 52g
- sugar: 2g
- protein: 31g

ingredients:
- 3-4 boneless skinless chicken breasts - cooked and diced
- 1 stalk of celery - chopped
- 1/2 white onion - diced
- 2 tsp minced garlic
- 1/2 cup shredded carrots
- 1 Tbsp olive oil
- 4 cups low sodium chicken broth
- 1 tsp thyme
- 16 ounces potato gnocchi
- 2 cups half and half
- 1 cup fresh spinach - roughly chopped
- Salt
- Pepper

directions:
- Heat olive oil in a large pot over medium heat. Add celery, onions, garlic, and carrots and saute for 2-3 minutes until onions are translucent. 
- Add chicken, chicken broth, salt, pepper, and thyme to slow cooker. Cook on low 3-4 hours. Add gnocchi for the last hour.
- Before serving, add the half and half and spinach and stir to combine. Cook another 5 minutes until spinach is tender. Taste, add salt and pepper if needed, and serve. 

---

Olive Garden Chicken Gnocchi Soup copycat is every bit as creamy and delicious as the restaurant version.