---

layout: recipe
title:  "One-Pan Mushroom Ravioli Gratin"
image: one-pan-mushroom-ravioli-gratin.jpg
imagecredit: https://www.hellofresh.com/recipes/one-pan-mushroom-ravioli-gratin-5a4e5c9bc94eac13412f2872
tags: mains, vegetarian, mushroom, hello fresh

ingredients:
- 8 oz button mushrooms
- 1/2 yellow onion
- 1/4 oz thyme
- vegetable stock concentrate
- 9 oz mushroom ravioli
- 1/4 cup panko breadcrumbs
- 1/4 cup parmesan cheese
- 4 Tbsp sour cream
- 2 tsp olive oil
- salt
- pepper

directions:
- Wash and dry all produce. Preheat broiler to high or oven to 500 degrees. Trim, then thinly slice mushrooms. Halve, peel, and thinly slice half the onion (use the rest as you like). Strip thyme leaves from stems; discard stems.
- Heat a drizzle of olive oil in a large pan over medium heat (use an ovenproof pan if you have one). Add mushrooms, onion, and thyme. Cook, tossing, until softened, 5-6 minutes. Season with salt and pepper. Stir in stock concentrate and 1½ cups water. Bring to a simmer.
- Add ravioli to pan, spreading them out in an even layer. Cover and cook 4 minutes. Uncover and cook, spooning stock over ravioli occasionally, until tender, 5-6 minutes more. If liquid evaporates before ravioli are tender, add a splash of water to pan; shake pan to keep ravioli from sticking.
- While ravioli cook, combine ¼ cup panko (we sent more), Parmesan, a drizzle of olive oil, and a pinch of salt and pepper in a small bowl.
- Add sour cream to pan and gently stir to coat ravioli and create a thick sauce. (If sauce seems dry, add a splash of water.) Season with salt and pepper. If your pan is not ovenproof, transfer mixture to a small baking dish at this point.
- Sprinkle crust over ravioli. Transfer pan to broiler (or oven) and broil until crust is golden and crisp, 1-2 minutes. Divide ravioli between plates and serve.

---

Gratin is a fancy French way of saying this dish is topped with cheesy breadcrumbs and heated under the broiler until brown and crispy. So it essentially translates to “delicious,” right? Especially when you factor in the tender ravioli and creamy sauce. But just in case you need another reason to give this recipe a shot, let us point out that you can make it in only one pot. Talk about an easy cleanup.
