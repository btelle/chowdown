---

layout: recipe
title:  "Pesto Tortellini Bake"
image: pesto-tortellini-bake.jpg
imagecredit: https://www.hellofresh.com/recipes/spring-tortellini-gratin-58b99790d56afa18711667a3
tags: mains, pasta, hello fresh
prep-time: 30 minutes

nutrition:
- servings: 2
- calories: 700
- fat: 34g
- carbohydrate: 66g
- sugar: 8g
- protein: 30g
- sodium: 1100mg

ingredients:
- 9 oz cheese tortellini
- 1 cup 2% milk
- 1/4 cup parmesan cheese
- 2 oz pesto
- 6 oz asparagus
- 2 clove garlic
- 1/4 cup panko breadcrumbs
- vegetable stock concentrate
- 2 tsp olive oil
- salt
- pepper

directions:
- Wash and dry all produce. Preheat broiler to high or oven to 500 degrees. Thinly slice garlic. Trim and discard bottom inch from asparagus, then cut stalks into 2-inch pieces.
- Heat a drizzle of olive oil in a large pan over medium heat (use an ovenproof pan if you have one). Add asparagus and toss until slightly softened, 1-2 minutes. Add garlic and toss until fragrant, about 1 minute. Season with salt and pepper.
- Add milk, stock concentrate, and pesto to pan and stir to combine. Add tortelloni, spreading them out in a single layer. Bring mixture to a boil, then lower heat and let simmer until sauce thickens and tortelloni are tender, 5-7 minutes, stirring occasionally. Add a splash of water if pan seems dry.
- While tortelloni simmer, combine panko, Parmesan, and a drizzle of olive oil in a small bowl. Season with salt and pepper
- Remove pan from heat. (Transfer mixture to small baking dish at this point if your pan isn’t ovenproof.) Sprinkle crust mixture over tortelloni, covering them as evenly as possible.
- Place pan or dish under broiler (or in oven). Remove once crust is toasty and sauce is bubbly, 1-2 minutes. Divide tortelloni between bowls and serve.

---

This skillet pasta bake truly is one-pan cooking at its finest—even the sauce thickens in the pan while the tortelloni cook. Although it’s got plenty of creamy, comforting elements, this dish also has tons of green to keep things fresh. Asparagus spears are folded in to keep things snappy, while basil pesto adds an herby touch throughout.
