---

layout: recipe
title:  "Pico De Gallo Salsa"
image: pico-de-gallo.jpg
imagecredit: http://www.salsaandsalsa.com/recipe.pdf
tags: sides
prep-time: 10 mins

ingredients:
- 2 roma tomatoes
- 1/2 white onions, diced
- 1/2 serrano chile, de-seeded and diced
- pinch of cilantro
- lime juice
- salt

directions:
- Mix all ingredients, add a few drops of lime, pinch of cilantro, salt to taste.

---
