---

layout: recipe
title:  "Pizza Toast"
image: pizza-toast.jpg
imagecredit: https://www.tiktok.com/@simplydeliciousfood/video/7226017748940377349
tags: mains, sides, air fryer
prep-time: 10 mins

ingredients:
- sourdough bread
- shredded mozzarella cheese
- pizza sauce
- pepperoni
- fresh basil
- olive oil

directions:
- Drizzle sliced bread with olive oil and air fry until crispy, appx 3 minutes
- Spread pizza sauce on bread, followed by mozzarella and pepperoni. Air fry until cheese is melted, andother 3-5 minutes.
- Top with fresh basil.

---
