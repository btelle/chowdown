---

layout: recipe
title:  "Presto Pesto Panko Chicken"
image: presto-pesto-panko-chicken.jpg
imagecredit: https://www.hellofresh.com/recipes/w19-r1-58d3c690d0d6bd13d10bce72
tags: mains, chicken, pesto, hello fresh

ingredients:
- 12 oz yukon gold potatoes
- 12 oz chicken breasts
- 1/2 cup panko breadcrumbs
- 2 Tbsp pesto
- 2 oz arugula
- 1/2 cup mozzarella cheese
- 1 lemon
- olive oil
- salt
- pepper

directions:
- Wash and dry all produce. Preheat oven to 450 degrees. Cut potatoes into 1/2-inch cubes.
- Toss potatoes on a baking sheet with a drizzle of olive oil and a pinch of salt and pepper. Roast in oven until browned, 20-25 minutes, tossing halfway through.
- In a small bowl, combine panko, mozzarella, a drizzle of olive oil, and a pinch of salt and pepper.
- Place chicken breasts on a lightly oiled baking sheet. Brush 2 TBSP pesto (we sent more) onto tops. Press crust mixture into pesto to adhere. Roast in oven until chicken is no longer pink in center, about 20 minutes.
- Halve lemon. In a large bowl, toss together arugula, a squeeze of lemon, and a large drizzle of olive oil. Season with salt and pepper.
- Divide potatoes and chicken between plates. Serve with salad on the side.

---

Pesto: you’ve had it on pasta or maybe as a dip. Let us introduce you to yet another way to use this Italian favorite. Here, we’re spreading it onto oven-roasted chicken breasts, using it as a glue to hold down a layer of panko breadcrumbs. That way, you get herby aromatics, meaty juices, and crispy golden goodness in every bite.
