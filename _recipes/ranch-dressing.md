---

layout: recipe
title:  "Ranch Dressing"
image: ranch-dressing.jpg
imagecredit: https://www.tiktok.com/@stuffedchells/video/7207981525168721195
tags: salads
prep-time: 5 mins

ingredients:
- 2 packets Hidden Valley restaurant-style ranch dressing mix
- 1 cup buttermilk
- 1/2 cup McCormick Mayonesa
- 2 Tbsp sour cream
- 2 tsp dill
- 1 clove crushed garlic

directions:
- Combine all ingredients in a mixing bowl and whisk until well combined and smooth. Store in a air-tight container.

---

This dressing will keep, refrigerated, for up to 2 weeks.
