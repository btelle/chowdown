---

layout: recipe
title:  "Rotisserie Chicken Bowl"
image: rotisserie-chicken-bowl.jpg
tags: mains, rotisserie chicken, meat

ingredients:
- 1/4 cup water
- 2 Tbsp lime juice
- 1/2 tsp chili powder
- 1/4 tsp cumin
- 1/4 tsp pepper
- 1/8 tsp red pepper
- 2 cups rotisserie chicken
- 1 cup white rice
- 1 can black beans
- shredded cheese
- salsa
- sour cream

directions:
- In a pot, cook rice with 2 cups water to a boil, then reduce heat to low and simmer for 15 minutes. Remove from heat and let sit for 5 minutes. Or use a rice cooker.
- Carve the rotisserie chicken, cutting up about 2 cups worth into 1/2 inch cubes.
- In a small saucepan, combine the water, lime juice, and dry ingredients. Heat over medium-high heat until warm, about 3 minutes.
- Add the chicken to the saucepan and cook, stirring regularly, until chicken is hot and liquid is absorbed. Remove from heat.
- While the chicken is cooking, empty beans into a microwave-safe bowl and heat on high for 2:30.
- Split rice, beans and chicken between two bowls. Top with cheese, salsa, and sour cream.

---