---

layout: recipe
title:  "Salmon Pesto Bagel Sandwich"
image: salmon-pesto-bagel-sandwich.jpg
tags: breakfast, bagel
prep-time: 10 mins

ingredients:
- bagel/bread
- cream cheese
- vegan pesto (Trader Joes)
- smoked salmon (or egg)
- pickled onions
- broccoli sprouts

directions:
- Toast bagel.
- On one side, spread cream cheese. On the other, spread pesto.
- Make a sandwich with smoked salmon, pickled onions, and sprouts.

---
