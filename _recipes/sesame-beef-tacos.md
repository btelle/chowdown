---

layout: recipe
title:  "Sesame Beef Tacos"
image: sesame-beef-tacos.jpg
imagecredit: https://www.hellofresh.com/recipes/sesame-beef-tacos-5a14555e2c9e8648a24c33f2
tags: mains, beef, hello fresh

ingredients:
- 10 oz ground beef
- 6 flour tortillas
- 2 scallions
- 1 cucumber
- 3 radishes
- 1/4 oz cilantro
- 2 Tbsp white wine vinegar
- 4 Tbsp sour cream
- 1 tsp korean chili flakes
- 4 oz red cabbage
- 1 tsp garlic
- 1 Tbsp soy sauce
- 6 tsp sriracha
- 1 Tbsp sesame oil
- 1 tsp vegetable oil
- 2 tsp sugar
- salt
- pepper

directions:
- Wash and dry all produce. Trim, then thinly slice scallions. Thinly slice cucumber and radishes crosswise. Pick cilantro leaves from stems; discard stems.
- In a medium bowl, toss cucumber, radishes, white wine vinegar, 1 tsp sugar, and a pinch of salt and pepper. Set aside until rest of meal is ready.
- In a small bowl, stir together sour cream and a few chili flakes (to taste— start with a pinch and go up from there, saving a little for the filling). Season with salt and pepper.
- Heat a drizzle of oil in a large pan over medium-high heat. Add scallions and cabbage. Cook, tossing, until softened, 4-5 minutes. Add beef, garlic powder, and 1 tsp sugar, breaking up meat into pieces. Cook, tossing, until beef is no longer pink, 4-5 minutes. Season with salt, pepper, and up to ½ tsp more sugar (as needed). Stir in soy sauce, 1 tsp sriracha (we sent more), sesame oil and a pinch of chili flakes (to taste).
- While filling cooks, wrap tortillas in a damp paper towel and microwave on high until warm, about 30 seconds. Alternatively, wrap tortillas in foil and warm in oven for 5 minutes at 425 degrees.
- Spread a bit of chili crema on each tortilla, then top with filling, a small amount of pickled veggies, cilantro, sriracha, and any remaining chili flakes, if desired. Serve with remaining pickled veggies on the side.
---

Instead of going the traditional route with these tacos, we decided to mix things up with a Korean-style sesame beef filling. The filling gets a kick from gochugaru—chili flakes traditionally used in dishes like kimchi and bulgogi. It’s an unconventional choice, perhaps, but one that lives up to the maxim that everything tastes better in a tortilla.
