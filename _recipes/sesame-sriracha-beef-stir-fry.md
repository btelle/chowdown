---

layout: recipe
title:  "Sesame Sriracha Beef Stir-Fry"
image: sesame-sriracha-beef-stir-fry.jpg
imagecredit: https://www.hellofresh.com/recipes/sesame-sriracha-beef-stir-fry-5a835ff230006c1d735decd2
tags: mains, meat, hellofresh

ingredients:
- 1 carrot
- 2 green onion
- 1/2 cup jasmine rice
- 3 Tbsp soy sauce
- 2 tsp sriracha
- 1 Tbsp sesame seeds
- 2 clove garlic
- 1 lime
- 1 Tbsp sesame oil
- 1 oz honey
- 10 oz ground beef
- olive oil
- salt
- pepper

directions:
- Wash and dry all produce. Bring ¾ cups water and a large pinch of salt to a boil in a small pot. Peel carrots. Using a vegetable peeler, shave carrots lengthwise into thin ribbons. Mince garlic. Trim, then thinly slice scallions, keeping greens and whites separate. Halve lime; cut one half into wedges.
- Once water is boiling, add rice to pot. Cover, lower heat, and reduce to a gentle simmer. Cook until tender, about 15 minutes. Keep covered off heat until meal is ready. Meanwhile, in a small bowl, combine 1½ tsp sesame oil, 1 TBSP soy sauce, 1 TBSP honey, and sriracha to taste (we’ll use more of the sesame oil, soy sauce, and honey later).
- Heat a drizzle of oil in a large pan over medium-high heat. Add carrots and cook, tossing, until tender, 2-3 minutes. Season with salt and pepper. Remove from pan and set aside.
- Heat another drizzle of oil in pan used for carrots over medium-high heat. Add garlic and scallion whites. Cook until fragrant, about 30 seconds. Add beef, breaking up meat into pieces. Cook until no longer pink, about 4 minutes. Increase heat to high and cook, stirring only once or twice, until brown and crispy in spots, about 3 minutes. Season with salt and pepper.
- Add carrots to pan with beef, along with remaining honey and soy sauce. Toss to combine. Remove pan from heat and stir in remaining sesame oil. Add a squeeze or two of lime (to taste). Season with salt and pepper, if needed (it may already be salty enough).
- Divide rice and stir-fry between bowls. Sprinkle with sesame seeds and scallion greens. Drizzle with sriracha mixture. Serve with lime wedges on the side for squeezing over.

---
