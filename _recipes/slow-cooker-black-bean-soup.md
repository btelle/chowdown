---

layout: recipe
title:  "Slow Cooker Black Bean Soup"
image: slow-cooker-black-bean-soup.jpg
imagecredit: https://www.budgetbytes.com/slow-cooker-black-bean-soup/
tags: mains, slow cooker, soup
prep-time: 8 hours

nutrition:
- servings: 4
- calories: 293
- fat: 2g
- carbohydrate: 55g
- protein: 17g

ingredients:
- 2 cloves garlic
- 1 yellow onion
- 2 ribs celery
- 2 carrots
- 1 lb. black beans (uncooked)
- 1 cup salsa
- 1 Tbsp chili powder
- 1/2 Tbsp ground cumin
- 1 tsp dried oregano
- 4 cups vegetable broth
- 2 cups water

directions:
- Mince the garlic, dice the onion and celery, and grate the carrots on a large holed cheese grater. Rinse the black beans in a colander under cool running water and pick out any stones or debris.
- Combine the garlic, onion, celery, carrots, black beans, salsa, chili powder, cumin, oregano, vegetable broth, and water in a 5-7 quart slow cooker. Stir well.
- Place the lid on the slow cooker and cook on high for 6-8 hours (you want the beans to get VERY soft). Once the beans are very soft, use an immersion blender to blend the soup until it is thick and creamy (leave some beans whole if desired). Taste the soup and add salt if needed (this will depend on the brand of vegetable broth used).

---

This awesomely flavorful and easy slow cooker black bean soup practically makes itself, which is perfect for hot summer days like this when you don’t want a pot of soup boiling away on the stove making your whole kitchen extra steamy.
