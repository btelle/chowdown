---

layout: recipe
title:  "Slow Cooker Meatballs"
image: slow-cooker-meatballs.jpg
imagecredit: https://www.lecremedelacrumb.com/3-ingredient-slow-cooker-sweet-n-spicy-party-meatballs/
tags: mains, meat, slow cooker

ingredients:
- 1 bag meatballs (80 meatballs)
- 2 12 oz jars Heinz chili sauce
- 1 32 oz jar grape jelly

directions:
- Add Trader Joe’s frozen meatballs, chili sauce and grape jelly to slow cooker.
- Cook on high for 3 hours.

---
