---

layout: recipe
title:  "Slow Cooker Ropa Vieja"
image: slow-cooker-ropa-vieja.jpg
imagecredit: https://littlespicejar.com/cuban-shredded-beef-slow-cooker/
tags: mains, slow cooker, beef, cuban
prep-time: 8 hours

nutrition:
- servings: 4
- calories: 165
- fat: 6g
- carbohydrate: 9.5g
- protein: 18g

ingredients:
- 2lb beef chuck roast or flank steak
- 6 cloves garlic
- 1 green bell pepper, thinly sliced
- 1 medium onion, thinly sliced
- 2 (8oz) cans tomato sauce
- 1 (4oz) can chopped green chilies
- 2 Tbsp tomato paste
- 2 tsp dried oregano
- 2 tsp salt
- 1 Tbsp ground cumin
- 1 Tbsp white vinegar
- 1/2 tsp red pepper flakes
- 1 Tbsp oil

directions:
- Take the beef chuck roast and using a small paring knife, make 3 small 'x' marks into the meat. Take 3 cloves of garlic and insert them into the 3 'x' marks so that the clove is completely inserted into the thickness of roast. Season both sides of the meat with a pinch of salt and pepper.
- Heat the oil in a large, deep skillet. Add the beef in one piece and fry on each side for 5 minutes, turning only when the exterior is browned. This will splatter a bit but don't cover the pan as the meat will end up steaming rather than searing.
- Chop the remaining 3 cloves of garlic into thin slices and add to the slow cooker along with all the remaining ropa vieja ingredients. Cook on low for 7-8 hours. When the beef falls apart easily when pulled with two forks, it's done.
- Shred the beef with the forks and give it a stir. Taste for seasonings, season with additional salt and pepper as desired.
- Serve over white rice with a side of Cuban black beans

---

Cuban Shredded beef or ropa vieja made in the slow cooker! We sear the meat and then braise it for hours with onions, garlic, and spices until the meat is just falling apart tender. Serve it up with black beans and rice!