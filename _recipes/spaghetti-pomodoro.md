---

layout: recipe
title:  "Spaghetti Pomodoro"
image: spaghetti-pomodoro.jpg
imagecredit: https://www.tiktok.com/@karens_cooking/video/7219780611156757803
tags: lunch
prep-time: 10 mins

ingredients:
- 1 28oz can San Marzano tomatoes
- spaghetti
- 4 cloves garlic
- fresh basil
- olive oil
- salt

directions:
- Heat a generous amount of olive oil in a large pot. Cook whole cloves of garlic in oil until brown on the outside. Remove garlic from the pot.
- Add whole can of tomatoes. Take the oil off the heat for a minute or two first to prevent splattering.
- Crush tomatoes with a wooden spoon. Let simmer for 20 minutes.
- Add a handful of basil and simmer. Season with salt.
- Add aldente pasta to the sauce and serve.

---
