---

layout: recipe
title:  "Spicy Red Salsa"
image: spicy-red-salsa.jpg
imagecredit: http://www.salsaandsalsa.com/recipe.pdf
tags: sides
prep-time: 10 mins

ingredients:
- 1-3 cloves roasted garlic
- 1/4 white onion roasted and chopped
- 1 roasted jalapeño or serrano chiles
- 2 roasted roma tomatoes
- pinch of cilantro
- lime juice
- salt

directions:
- Grind the garlic in a molcajete.
- Add chile, onions, tomatoes, a pinch of cilantro, salt and a few drops of lime juice. Continue mashing until well blended.

---
