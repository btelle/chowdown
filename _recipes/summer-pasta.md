---

layout: recipe
title:  "Summer Pasta"
image: summer-pasta.jpg
imagecredit: https://barefootcontessa.com/recipes/summer-garden-pasta
tags: mains, pasta
prep-time: 4 hrs

ingredients:
- 4 pints cherry tomatoes, halved
- 2 Tbsp minced garlic
- 18 large basil leaves, julienned
- 1/2 tsp crushed red pepper flakes
- 1/2 tsp pepper
- angel hair pasta
- 1 1/2 cups Parmesan cheese
- olive oil
- salt

directions:
- Combine the cherry tomatoes, 1/2 cup olive oil, garlic, basil leaves, red pepper flakes, 1 teaspoon salt, and the pepper in a large bowl. Cover with plastic wrap, and set aside at room temperature for about 4 hours.
- Just before serving, add drained al dente angel hair to the bowl with the cherry tomatoes.
- Add the cheese and some extra fresh basil leaves and toss well.
- Serve in big bowls with extra cheese on each serving.

---
