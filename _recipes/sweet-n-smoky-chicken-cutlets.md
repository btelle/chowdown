---

layout: recipe
title:  "Sweet 'n' Smoky Chicken Cutlets"
image: sweet-n-smoky-chicken-cutlets.jpg
imagecredit: https://www.hellofresh.com/recipes/sweet-n-smoky-chicken-cutlets-5added59ae08b54bd0480272
tags: mains, chicken, hello fresh
prep-time: 30 minutes

nutrition:
- servings: 2
- calories: 620
- fat: 24g
- carbohydrate: 67g
- sugar: 23g
- protein: 36g
- sodium: 250mg

ingredients:
- 10 oz chicken cutlets
- 2 scallions
- 4 oz pineapple
- 6 oz green beans
- 1 chili pepper
- 1/2 cup basmati rice
- 2.5 tsp white wine vinegar
- 1 oz cherry jam
- 1 tsp chili powder
- 1 Tbsp sweet and smoky bbq seasoning
- chicken stock concentrate
- 3 Tbsp butter
- 1 tsp vegetable oil
- 2 tsp sugar
- salt
- pepper

directions:
- Wash and dry all produce. Pat chicken dry with paper towel. Season all over with salt, pepper, and ½ tsp chili powder (save the rest for later). Trim, then thinly slice scallions, separating greens and whites. Drain pineapple over a small bowl, reserving juice. Halve Thai chili lengthwise, then thinly slice. Remove ribs and seeds for less heat.
- Melt 1 TBSP butter in a small pot over medium-high heat. Add pineapple chunks and scallion whites. Cook, tossing occasionally, until just softened, about 1 minute. Stir in rice, 1 cup water, and salt and pepper to taste. Bring to a boil, then reduce heat to low, cover, and let come to a gentle simmer. Cook until tender, 12-15 minutes, then let stand off heat, covered, for about 5 minutes.
- Heat a drizzle of oil in a large pan over medium-high heat (use a nonstick pan if you have one). Add chicken and cook until browned on bottom, 5-6 minutes. Flip and cook until browned on other side, 3-4 minutes more. Meanwhile, add barbecue seasoning, remaining chili powder, 2 tsp sugar, 2 1/2 tsp vinegar (we sent more), and jam to pineapple juice in bowl and stir. 
- Place green beans in a medium, microwave-safe bowl with 2 TBSP water. Cover bowl with plastic wrap and poke a few holes in wrap. Microwave on high until tender but still bright green and a little crisp, 2-3 minutes. Drain and toss with 1 TBSP butter. Season with salt and pepper.
- Once chicken is browned on both sides, add jam mixture and stock concentrate to same pan. Flip chicken to coat all over. Let jam and stock mixture simmer until thick and glaze-like, 1-2 minutes. (It’s ready when it coats the back of a spoon.) Remove pan from heat and add 1 TBSP butter, stirring to melt. Season with salt and pepper.
- Fluff rice with a fork, then divide between plates along with chicken and green beans. Drizzle any remaining glaze in pan over chicken. Garnish with scallion greens. Sprinkle Thai chili over chicken (to taste — leave it off if you’re not a fan of spicy heat).

---

Barbecue chicken just got even better thanks to a few secret ingredients from our chefs. These thinly-sliced chicken cutlets are coated in a sauce that combines barbecue spices with cherry jam, pineapple juice, and ancho chili powder. The result is something as succulently sweet as it is smoky. Plus, it pairs brilliantly with buttered green beans and the pineapple rice that continues the fruit theme.
