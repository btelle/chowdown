---

layout: recipe
title:  "\"Swimmer Girl's\" Tuna Salad"
image: swimmer-tuna.jpg
imagecredit: https://www.tiktok.com/@anna_talcott/video/6979581332154305798
tags: lunch
prep-time: 10 mins

ingredients:
- 1 can tuna
- 1/2 cup celery, finely chopped
- 1/2 cup carrot, finely chopped
- 1/2 cup red onion, finely chopped
- 1/2 cup zucchini, finely chopped
- capers
- 1 tsp caper brine
- lemon juice
- olive oil
- salt
- pepper

directions:
- Combine all ingredients in a mixing bowl and toss until well mixed. Serve with crackers or toasted pita

---
