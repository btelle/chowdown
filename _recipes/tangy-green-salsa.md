---

layout: recipe
title:  "Tangy Green Salsa"
image: tangy-green-salsa.jpg
imagecredit: http://www.salsaandsalsa.com/recipe.pdf
tags: sides
prep-time: 10 mins

ingredients:
- 1-3 cloves roasted garlic
- 1/4 white onion roasted and chopped
- 1 serrano chile
- 4 roasted tomatillos
- pinch of cilantro
- lime juice
- salt

directions:
- Grind the garlic in a molcajete.
- Add chile, onions, tomatillos, a pinch of cilantro, salt and a few drops of lime juice. Continue mashing until well blended.

---
