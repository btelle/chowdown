---

layout: recipe
title:  "Tiktok White Chicken Chili"
image: tiktok-white-chicken-chili.png
imagecredit: https://www.tiktok.com/@bysophialee/video/7009830297285987590
tags: mains, slow cooker, chili
prep-time: 4 hours

nutrition:
- servings: 4
- calories: 368
- fat: 5g
- carbohydrate: 47g
- sugar: 5g
- protein: 33g

ingredients:
- 2 Chicken Breast
- 32 oz Chicken Broth
- 2 cans northern beans
- 2 cans green chiles
- 1 yellow onion
- 3 spoonfuls minced garlic
- 1 tsp Cumin
- 1 tsp Oregano
- 1/4 tsp Cayenne Pepper
- Salt
- Pepper
- Sour Cream

directions:
- Put chicken, chopped onion, all canned ingredients and spices in crock pot
- Pour chicken broth over and stir
- Cook on high 3-4 hours
- Shred chicken, put back in crock pot
- Put a cup or two of broth in a bowl and gently stir in sour cream
- Pour sour cream mixture back into crock pot
- Serve with tortilla chips, fritos, cheese, sour cream

---

by far the best crockpot meal 🤩 #fyp #foryoupage #crockpotmeals #dinnerideas #crockpot #crockpotdinners #crockpotrecipes #IDeserveTuitionContest