---

layout: recipe
title:  "Veggie & Tofu Chilaquiles"
image: veggie-tofu-chilaquiles.jpg
tags: mains, breakfast, spicy
prep-time: 30 minutes

nutrition:
- servings: 4
- calories: 304
- fat: 11g
- carbohydrate: 39g
- sugar: 5g
- protein: 12g

ingredients:
- 12 corn tortillas
- 1 block medium-firm tofu
- 2 tsp soy sauce
- 1 tsp garlic powder
- 1/4 cup nutritional yeast
- 1/2 medium onion, chopped
- 1 bell pepper, chopped
- 1-2 jalepenos, chopped
- 2 cloves garlic, minced
- 2-3 cups fresh spinach
- 2 1/2 cups salsa verde
- 1/4 cup vegetable broth (or water)

directions:
- Set oven to 400F. Cut tortillas in 8 pieces like chips. Spread on baking sheet and dry in oven for 15-20 mins, stirring once.
- In a big skillet, heat 1 tsp olive oil over medium heat. Crumble in tofu, soy sauce and garlic powder and cook 2 mins.
- Stir in nutritional yeast and set aside
- Wipe pan and add 1 tsp olive oil. Cook onion, bell pepper and jalepeno for 3-5 mins. Add spinach and cook for 30 more seconds.
- Add half of the dried tortillas, 1 cup of salsa and half the broth/water and mix it all together.
- Add half of the tofu, layer on the rest of the tortillas, then the rest of the tofu, salsa and broth. Stir gently to ensure all tortillas are moist.
- Let simmer for 5 minutes, unlit most of the liquid is evaporated.
- Serve hot with jalepenos, avocado and cilantro for garnish.

---

Veggie and tofu chilaquiles from the THUG Kitchen cookbook. This dish is very spicy. It also makes A LOT and it's not great reheated. Recommend halving.
